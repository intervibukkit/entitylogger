package ru.intervi.entitylogger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;

public class Search {
	public Search(Main main) {
		this.main = main;
	}
	
	private Main main;
	private volatile HashMap<CommandSender, List<String>> map = new HashMap<CommandSender, List<String>>();
	
	public List<String> search(Location loc, int rad, String type, String date, boolean splitted) {
		String world = loc.getWorld().getName();
		int x = loc.getBlockX(), y = loc.getBlockY(), z = loc.getBlockZ();
		ArrayList<String> result = new ArrayList<String>();
		BufferedReader reader = null;
		ArrayList<String> sd = new ArrayList<String>();
		if (date != null && !date.isEmpty()) {
			String split[] = date.split(" ");
			for (String s : split[0].split("-")) sd.add(s);
			for (String s : split[1].split(":")) sd.add(s);
		}
		int day = Integer.parseInt(sd.get(0));
		int month = Integer.parseInt(sd.get(1));
		int year = Integer.parseInt(sd.get(2));
		int hour = Integer.parseInt(sd.get(3));
		int minute = Integer.parseInt(sd.get(4));
		int second = Integer.parseInt(sd.get(5));
		String logs[] = new File(main.PATH).list();
		for (String path : logs) {
			path = main.PATH + File.separatorChar + path;
			if (!new File(path).isFile()) continue;
			try {
				reader = new BufferedReader(new FileReader(path));
				while(reader.ready()) {
					if (result.size() > main.conf.limit) return result;
					String line = reader.readLine();
					if (line == null || line.isEmpty()) continue;
					String ltype = line.substring(line.indexOf("Type:")).trim();
					ltype = ltype.split(" ")[1];
					if (type != null && !type.equalsIgnoreCase(ltype)) continue;
					String locstring = line.substring(line.indexOf("Location:"), line.indexOf("Type:"));
					locstring = locstring.substring(10).trim();
					String lworld = locstring.substring(0, locstring.indexOf(':'));
					if (!world.equals(lworld)) continue;
					String costring[] = locstring.substring(locstring.indexOf(':')+1).trim().split(" ");
					int lx = Integer.parseInt(costring[0]);
					int ly = Integer.parseInt(costring[1]);
					int lz = Integer.parseInt(costring[2]);
					if (Math.abs(x-lx) > rad || Math.abs(y-ly) > rad || Math.abs(z-lz) > rad) continue;
					ArrayList<String> d = new ArrayList<String>();
					String dt[] = line.split("->")[0].substring(6).trim().split(" ");
					for (String s : dt[0].split("-")) d.add(s);
					for (String s : dt[1].split(":")) d.add(s);
					int lday = Integer.parseInt(d.get(0));
					int lmonth = Integer.parseInt(d.get(1));
					int lyear = Integer.parseInt(d.get(2));
					int lhour = Integer.parseInt(d.get(3));
					int lminute = Integer.parseInt(d.get(4));
					int lsecond = Integer.parseInt(d.get(5));
					if (lday < day || lmonth < month || lyear < year ||
							lhour < hour || lminute < minute || lsecond < second) continue;
					line = line.substring(7);
					if (splitted) result.add(line.substring(0, line.indexOf("Location:")).trim());
					else result.add(line);
				}
			} catch(IOException e) {e.printStackTrace();}
			finally {
				try {reader.close();}
				catch(IOException e) {e.printStackTrace();}
			}
		}
		return result;
	}
	
	public class AsyncSearch extends Thread {
		public AsyncSearch(CommandSender sender, Location loc, int rad, String type, String date, boolean splitted) {
			this.sender = sender;
			this.loc = loc;
			this.rad = rad;
			this.type = type;
			this.date = date;
			this.splitted = splitted;
		}
		
		private CommandSender sender;
		private Location loc;
		private int rad;
		private String type;
		private String date;
		private boolean splitted;
		
		@Override
		public void run() {
			List<String> result = search(loc, rad, type, date, splitted);
			if (map.containsKey(sender)) map.remove(sender);
			map.put(sender, result);
			sender.sendMessage(main.conf.result);
		}
	}
	
	public void asyncSearch(CommandSender sender, Location loc, int rad, String type, String date, boolean splitted) {
		AsyncSearch async = new AsyncSearch(sender, loc, rad, type, date, splitted);
		async.start();
		sender.sendMessage(main.conf.search);
	}
	
	public void clear() {
		map.clear();
	}
	
	public void remove(CommandSender sender) {
		map.remove(sender);
	}
	
	public List<String> getPage(CommandSender sender, int page) {
		if (!map.containsKey(sender)) return null;
		if (page <= 0) page = 1;
		int pages = getPages(sender);
		if (page > pages) page = pages;
		List<String> log = map.get(sender);
		int to = page*10-1;
		if (to >= log.size()) to = log.size() != 0 ? log.size()-1 : 0;
		int from = to-9;
		if (from < 0) from = 0;
		ArrayList<String> result = new ArrayList<String>();
		result.add("========== " + String.valueOf(page) + " / " + String.valueOf(pages) + " ==========");
		if (to > 0) result.addAll(log.subList(from, to));
		return result;
	}
	
	public int getPages(CommandSender sender) {
		if (!map.containsKey(sender)) return 0;
		return (int) Math.ceil(map.get(sender).size()/10);
	}
	
	public static boolean isValidDate(String date) {
		return date.matches("^\\d{2}-\\d{2}-\\d{4} \\d{2}:\\d{2}:\\d{2}$");
	}
}
