package ru.intervi.entitylogger;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.Bukkit;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.io.File;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.List;

public class Main extends JavaPlugin implements Listener {
	private Logger logger = Logger.getLogger("EntityLogger");
	Config conf = new Config(this);
	final String PATH = getDataFolder().getAbsolutePath() + File.separatorChar + "logs";
	private Search search = new Search(this);
	
	@Override
	public void onLoad() {
		try {
			File file = new File(PATH);
			if (!file.isDirectory()) file.mkdirs();
			String path = file.getAbsolutePath() + File.separatorChar + "entity.log";
			FileHandler handler = new FileHandler(path, true);
			handler.setFormatter(new Formatter() {
				@Override
	            public String format(LogRecord record) {
	                SimpleDateFormat logTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	                Calendar cal = new GregorianCalendar();
	                cal.setTimeInMillis(record.getMillis());
	                //[INFO] 03-12-2017 15:32:32 -> ...
	                return "[" + record.getLevel() + "] " + logTime.format(cal.getTime()) + " -> " + record.getMessage() + "\n";
	            }
			});
			logger.addHandler(handler);
			logger.setUseParentHandlers(false);
		} catch(Exception e) {e.printStackTrace();}
	}
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable() {
		search.clear();
	}
	
	private String getItem(ItemStack item) {
		String result = item.getType().toString();
		if (result.equals("AIR")) return null;
		String amount = '(' + String.valueOf(item.getAmount()) + ')';
		String meta = "";
		if (item.hasItemMeta()) {
			meta += '(';
			ItemMeta imeta = item.getItemMeta();
			if (imeta.hasDisplayName()) meta += "Name: " + imeta.getDisplayName();
			if (imeta.hasEnchants()) {
				for (Entry<Enchantment, Integer> entry : imeta.getEnchants().entrySet())
					meta += '{' + entry.getKey().toString() + " [" + entry.getValue().toString() + "]}";
			}
			for (ItemFlag flag : imeta.getItemFlags()) meta += '[' + flag.toString() + ']';
			if (imeta.hasLore()) {
				meta += '[';
				for (String line : imeta.getLore()) meta += line + ';';
			}
		}
		result += amount;
		if (!meta.isEmpty()) result += " Meta: " + meta;
		return result;
	}
	
	private String getItem(List<ItemStack> items) {
		String result = "";
		for (ItemStack item: items) {
			if (item != null) {
				String stritem = getItem(item);
				if (stritem != null) result += stritem + ';';
			}
		}
		return result;
	}
	
	private String getInfo(Location loc, Entity entity) {
		String result = "Location: " + loc.getWorld().getName() + ": " + loc.getBlockX() + ' ' + loc.getBlockY() + ' ' + loc.getBlockZ();
		result += " Type: " + entity.getType().toString();
		String cname = entity.getCustomName();
		if (cname != null) result += " CustomName: " + cname;
		String name = entity.getName();
		if (name != null) result += " Name: " + name;
		return result;
	}
	
	private String getInfo(Location loc, LivingEntity entity) {
		String result = getInfo(loc, (Entity) entity);
		Player killer = entity.getKiller();
		if (killer != null) result += " Killer: " + killer.getName();
		EntityEquipment ee = entity.getEquipment();
		ArrayList<ItemStack> items = new ArrayList<ItemStack>();
		for (ItemStack item : ee.getArmorContents()) items.add(item);
		items.add(ee.getBoots());
		items.add(ee.getChestplate());
		items.add(ee.getHelmet());
		items.add(ee.getItemInMainHand());
		items.add(ee.getItemInOffHand());
		items.add(ee.getLeggings());
		String inv = getItem(items);
		if (!inv.isEmpty()) result += " Inventory: " + inv;
		return result;
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onSpawn(ItemSpawnEvent event) {
		if ((conf.cancel && !event.isCancelled()) || !conf.itemspawn ||
				conf.types.contains(event.getEntity().getType().toString())) return;
		String mess = "Spawn entity: " + getInfo(event.getLocation(), event.getEntity());
		if (event.isCancelled()) mess += " CANCEL";
		logger.info(mess);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onDespawn(ItemDespawnEvent event) {
		if ((conf.cancel && !event.isCancelled()) || !conf.itemdespawn ||
				conf.types.contains(event.getEntity().getType().toString())) return;
		String mess = "Despawn entity: " + getInfo(event.getLocation(), event.getEntity());
		if (event.isCancelled()) mess += " CANCEL";
		logger.info(mess);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onDeath(EntityDeathEvent event) {
		if (!conf.death || conf.types.contains(event.getEntity().getType().toString())) return;
		String mess = "Death entity: " + getInfo(event.getEntity().getLocation(), (LivingEntity) event.getEntity());
		String drop = getItem(event.getDrops());
		if (!drop.isEmpty()) mess += " Drop: " + drop;
		logger.info(mess);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onCSpawn(CreatureSpawnEvent event) {
		String reason = event.getSpawnReason().toString();
		if ((conf.cancel && !event.isCancelled()) || !conf.itemspawn ||
				conf.types.contains(event.getEntity().getType().toString()) || conf.reason.contains(reason)) return;
		String mess = "Creature spawn: Reason: " + reason + ' ' + getInfo(event.getLocation(), (LivingEntity) event.getEntity());
		if (event.isCancelled()) mess += " CANCEL";
		logger.info(mess);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onQuit(PlayerQuitEvent event) {
		search.remove(event.getPlayer());
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args == null || args.length == 0) {
			for (String s : conf.help) sender.sendMessage(s);
			return true;
		}
		boolean hasplayer = false;
		if (sender instanceof Player) hasplayer = true;
		switch(args[0].toLowerCase()) {
		case "reload":
			if (sender.hasPermission("entitylogger.reload")) {
				conf.load();
				sender.sendMessage(conf.reload);
			} else sender.sendMessage(conf.noperm);
			break;
		case "help":
			for (String s : conf.help) sender.sendMessage(s);
			break;
		case "search":
			if (sender.hasPermission("entitylogger.search")) {
				if ((hasplayer && args.length < 3) || (!hasplayer && args.length < 10)) {
					for (String s : conf.help) sender.sendMessage(s);
					break;
				}
				String date = args[1] + ' ' + args[2];
				if (!Search.isValidDate(date)) {
					for (String s : conf.help) sender.sendMessage(s);
					break;
				}
				int rad = 30;
				String type = null;
				boolean splitted = false;
				String world = null;
				int x = 0, y = 0, z =  0;
				Location loc = null;
				if (args.length >= 4 && args[3].matches("^[0-9]*$")) rad = Integer.parseInt(args[3]);
				if (args.length >= 5 && args[4].matches("^[a-zA-Z]*$") && !args[4].equalsIgnoreCase("ALL")) type = args[4];
				if (args.length >= 6 && (args[5].equals("true") || args[5].equals("false"))) splitted = Boolean.parseBoolean(args[5]);
				if (args.length >= 7 && Bukkit.getWorld(args[6]) != null) world = args[6];
				if (args.length >= 8 && args[7].matches("^[0-9]*$")) x = Integer.parseInt(args[7]);
				if (args.length >= 9 && args[8].matches("^[0-9]*$")) y = Integer.parseInt(args[8]);
				if (args.length >= 10 && args[9].matches("^[0-9]*$")) z = Integer.parseInt(args[9]);
				if (world != null && (x != 0 || y != 0 || z != 0)) loc = new Location(Bukkit.getWorld(world), (double) x, (double) y, (double) z);
				if (hasplayer) { //03-12-201 15:22:07 rad type splitted
					if (loc == null) loc = ((Player) sender).getLocation();
					search.asyncSearch(sender, loc, rad, type, date, splitted);
				} else { //03-12-201 15:22:07 rad type splitted world x y z
					if ((args.length >= 7 && args.length < 10) || loc == null) {
						for (String s : conf.help) sender.sendMessage(s);
						break;
					}
					search.asyncSearch(sender, loc, rad, type, date, splitted);
				}
			} else sender.sendMessage(conf.noperm);
			break;
		case "result":
			if (sender.hasPermission("entitylogger.search")) {
				int page = 1;
				if (args.length >= 2 && args[1].matches("^[0-9]*$")) page = Integer.parseInt(args[1]);
				List<String> log = search.getPage(sender, page);
				if (log == null) sender.sendMessage(conf.notfound);
				else for (String s : log) sender.sendMessage(s);
			} else sender.sendMessage(conf.noperm);
			break;
		case "clear":
			if (sender.hasPermission("entitylogger.clear")) {
				search.clear();
				sender.sendMessage(conf.clear);
			} else sender.sendMessage(conf.noperm);
			break;
		default:
			return false;
		}
		return true;
	}
}
