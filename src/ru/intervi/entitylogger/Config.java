package ru.intervi.entitylogger;

import java.util.List;
import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
	public Config(Main main) {
		this.main = main;
		load();
	}
	
	private Main main;
	
	public List<String> types = new ArrayList<String>();
	public List<String> reason = new ArrayList<String>();
	public int limit = 10000;
	public boolean cancel = false;
	public boolean itemspawn = true;
	public boolean itemdespawn = true;
	public boolean death = true;
	public boolean creaturespawn = true;
	public String reload = "конфиг перезагружен";
	public String noperm = "нет прав";
	public String search = "идёт поиск...";
	public String result = "для просмотра результатов введите /elog result";
	public String clear = "память очищена";
	public String notfound = "не найдено";
	public List<String> help = new ArrayList<String>();
	
	private String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	private List<String> color(List<String> list) {
		ArrayList<String> result = new ArrayList<String>();
		for (String line: list) result.add(color(line));
		return result;
	}
	
	public void load() {
		main.saveDefaultConfig();
		main.reloadConfig();
		FileConfiguration conf = main.getConfig();
		types = conf.getStringList("types");
		reason = conf.getStringList("reason");
		limit = conf.getInt("limit");
		cancel = conf.getBoolean("cancel");
		itemspawn = conf.getBoolean("itemspawn");
		itemdespawn = conf.getBoolean("itemdespawn");
		death = conf.getBoolean("death");
		creaturespawn = conf.getBoolean("creaturespawn");
		reload = color(conf.getString("reload"));
		noperm = color(conf.getString("noperm"));
		search = color(conf.getString("search"));
		result = color(conf.getString("result"));
		clear = color(conf.getString("clear"));
		notfound = color(conf.getString("notfound"));
		help = color(conf.getStringList("help"));
	}
}
