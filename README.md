# EntityLogger #


**Для версий >= 1.11.2**

Возможно, работает на ранних версиях.


Логирует спаун и смерть сущностей. Логи находится в субдиректории плагина с названием "logs".


**Команды:**

* */elog help* - справка
* */elog reload* - перезагрузить конфиг
* */elog clear* - очистить память
* */elog result [страница]* - посмотреть результат поиска
* */elog search dd-MM-yyyy HH:mm:ss [rad] [type] [splitted] [world] [x] [y] [z]* - поиск

*rad* - радиус в блоках, *type* - тип моба, *splitted* - короткий вариант, *world* - мир, *z*, *y*, *z* - координаты


*Пример:*

* /elog search 29-12-2016 15:22:07 50 SHEEP false World 100 40 123

В консоли нужно указывать полный вариант.

[Типы мобов](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/EntityType.html) (а также *ALL*)


**Разрешения:**

* *entitylogger.reload* - право на */elog reload*
* *entitylogger.clear* - право на */elog clear*
* *entitylogger.search* - право на */elog search*
